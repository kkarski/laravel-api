<?php

namespace App\Console\Commands;

use App\Domain;
use Illuminate\Console\Command;

class PingDomain extends Command
{
    const PING_REGEX_TIME = '/time(=|<)(.*)ms/';
    const PING_TIMEOUT = 10;
    const PING_COUNT = 1;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ping:domain';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pings domains from the database';

    /**
     * First three letters of our operating system (uppercase).
     *
     * @var string
     */
    protected $os;

    /**
     * Create a new command instance.
     *
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->os = strtoupper(substr(PHP_OS, 0, 3));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $domains = Domain::all();

        foreach ($domains as $domain) {
            $data = $this->parsePing($domain->url);

            $domain->pings()->create($data);
        }
    }

    private function parsePing($url)
    {
        $data = [
            'time' => 0,
            'success' => true,
        ];

        $cmd = sprintf('ping -w %d -%s %d %s',
            self::PING_TIMEOUT,
            $this->os === 'WIN' ? 'n' : 'c',
            self::PING_COUNT,
            escapeshellarg($url)
        );

        exec($cmd, $output, $result);

        if (0 !== $result) {
            $data['success'] = false;

            return $data;
        }

        $pingResults = preg_grep(self::PING_REGEX_TIME, $output);

        if (empty($pingResults)) {
            $data['success'] = false;

            return $data;
        }

        $pingResult = array_shift($pingResults);

        preg_match(self::PING_REGEX_TIME, $pingResult, $matches);

        $data['time'] = floatval(trim($matches[2]));

        return $data;
    }
}

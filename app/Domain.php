<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = ['url'];

    public function pings()
    {
        return $this->hasMany('App\Ping');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
